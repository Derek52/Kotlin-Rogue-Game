package pizza.derektheprogrammer.kotlinrogue

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Intersector
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.viewport.FitViewport
import pizza.derektheprogrammer.kotlinrogue.entities.Fighter
import pizza.derektheprogrammer.kotlinrogue.entities.Player
import pizza.derektheprogrammer.kotlinrogue.scenes.ActionLog
import pizza.derektheprogrammer.kotlinrogue.utils.Comun

class PlayScreen : Screen{

    val batch : SpriteBatch

    val gamePort : FitViewport

    var gameState = Comun.GameState.PLAYERS_TURN

    val scaleX : Float
    val scaleY : Float

    val shapeRenderer : ShapeRenderer

    //val player : Entity
    val player : Player

    //val gameMap : GameMap
    val gameMap : GameMap

    val playerImg = Texture("badlogic.jpg")
    val grass = Texture("grass.png")

    val roomMaxSize = Comun.ROOM_MAX_SIZE
    val roomMinSize = Comun.ROOM_MIN_SIZE
    val maxRoomNumber = Comun.MAX_ROOM_NUMBER

    val actionLog : ActionLog

    init {
        batch = SpriteBatch()
        shapeRenderer = ShapeRenderer()
        gamePort = FitViewport(Comun.MAP_WIDTH, Comun.MAP_HEIGHT)
        gamePort.camera.position.set(Comun.MAP_WIDTH/2f, Comun.MAP_HEIGHT/2f, 0f)

        scaleX = Gdx.graphics.width / Comun.MAP_WIDTH
        scaleY = Gdx.graphics.height / Comun.MAP_HEIGHT

        //player = Entity(playerImg, Vector2(Comun.MAP_WIDTH / 2, Comun.MAP_HEIGHT / 2))
        player = Player(Vector2(Comun.MAP_WIDTH/2, Comun.MAP_HEIGHT/2), Vector2(.5f, .5f), Vector2(), fighter = Fighter(100, 100, 20, 20))

        gameMap = GameMap(Comun.MAP_WIDTH, Comun.MAP_HEIGHT)
        //gameMap.initializeTiles()
        gameMap.makeMap(maxRoomNumber, roomMinSize, roomMaxSize, Comun.MAP_WIDTH.toInt(), Comun.MAP_HEIGHT.toInt(), player)
        player.computeFOV(gameMap.tiles)

        actionLog = ActionLog(batch)
    }

    private fun handleInput() {
        if (Gdx.input.isKeyPressed(Input.Keys.A) || Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            player.velocity.x = -Comun.PLAYER_SPEED
        }
        else if (Gdx.input.isKeyPressed(Input.Keys.D) || Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            player.velocity.x = Comun.PLAYER_SPEED
        }
        else {
            player.velocity.x = 0f
        }

        if (Gdx.input.isKeyPressed(Input.Keys.W) || Gdx.input.isKeyPressed(Input.Keys.UP)) {
            player.velocity.y = Comun.PLAYER_SPEED
        }
        else if (Gdx.input.isKeyPressed(Input.Keys.S) || Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            player.velocity.y = -Comun.PLAYER_SPEED
        }
        else {
            player.velocity.y = 0f
        }

        //here I leverage Kotlin's when statement, to make the code above, a bit more concise.
        //I do this to add vim arrow key movement, and to be an elitist bastard, I give the player a speed bonus for using vim keys
        when {
            Gdx.input.isKeyPressed(Input.Keys.H) -> player.velocity.x = -Comun.PLAYER_SPEED * 1.4f
            Gdx.input.isKeyPressed(Input.Keys.L) -> player.velocity.x = Comun.PLAYER_SPEED * 1.4f
        }

        when {
            Gdx.input.isKeyPressed(Input.Keys.J) -> player.velocity.y = -Comun.PLAYER_SPEED * 1.4f
            Gdx.input.isKeyPressed(Input.Keys.K) -> player.velocity.y = Comun.PLAYER_SPEED * 1.4f
        }


        if (Gdx.input.isKeyJustPressed(Input.Keys.R)) {
            gameMap.makeMap(maxRoomNumber, roomMinSize, roomMaxSize, Comun.MAP_WIDTH.toInt(), Comun.MAP_HEIGHT.toInt(), player)
        }
    }

    private fun update(dt : Float) {
        handleInput()
        if (gameState == Comun.GameState.PLAYERS_TURN) {
            player.update(dt, gameMap)
            gameState = Comun.GameState.ENEMY_TURN
        }
        if (gameState == Comun.GameState.ENEMY_TURN) {
            for (entity in gameMap.entities) {
                entity.update(dt, gameMap, player)
                entity.ai?.let {
                    entity.ai.takeTurn(player)
                }
            }
            gameState = Comun.GameState.PLAYERS_TURN
        }

        gameMap.update()
        actionLog.update(player.fighter.hp)
    }

    override fun render(delta: Float) {
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        update(delta)

        gamePort.camera.update()

        //here I just wanted to draw the viewport to make sure It was setup properly. The shapeRenderer is very useful for debugging though.
        shapeRenderer.projectionMatrix = gamePort.camera.combined
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled)

        for (x in 0 until Comun.MAP_WIDTH.toInt()) {
            for (y in 0 until Comun.MAP_HEIGHT.toInt()) {
                if (gameMap.tiles[x][y].illuminated) {
                    if (gameMap.tiles[x][y].blocked) {
                        shapeRenderer.setColor(1f, 0f, 0f, 1f) //illuminated Wall
                        shapeRenderer.rect(gameMap.tiles[x][y].posX, gameMap.tiles[x][y].posY, gameMap.tiles[x][y].rect.width, gameMap.tiles[x][y].rect.height)
                    } else {
                        shapeRenderer.setColor(0f, 1f, 0f, 1f) //illuminated floor
                        shapeRenderer.rect(gameMap.tiles[x][y].posX, gameMap.tiles[x][y].posY, gameMap.tiles[x][y].rect.width, gameMap.tiles[x][y].rect.height)
                    }
                } else if (gameMap.tiles[x][y].visible) {
                    if (gameMap.tiles[x][y].blocked) {
                        shapeRenderer.setColor(0f, 0f, 0.39f, 1f) //blocked color
                        shapeRenderer.rect(gameMap.tiles[x][y].posX, gameMap.tiles[x][y].posY, gameMap.tiles[x][y].rect.width, gameMap.tiles[x][y].rect.height)
                    } else {
                        shapeRenderer.setColor(0.19f, 0.19f, 0.58f, 1f) //not blocked color
                        shapeRenderer.rect(gameMap.tiles[x][y].posX, gameMap.tiles[x][y].posY, gameMap.tiles[x][y].rect.width, gameMap.tiles[x][y].rect.height)
                    }
                }
            }
        }

        shapeRenderer.setColor(1f, 1f, 0f, 1f)
        for (entity in gameMap.entities) {
            entity.ai?.let {
                shapeRenderer.circle(entity.ai.fovCircle.x, entity.ai.fovCircle.y, entity.ai.fovCircle.radius)
            }
        }

        shapeRenderer.end()

        batch.projectionMatrix = gamePort.camera.combined
        batch.begin()

        for (entity in gameMap.entities) {
            if (Intersector.overlaps(player.fovCircle, entity.hitBox)) {
                entity.draw(batch)
            }
        }

        batch.draw(playerImg, player.position.x, player.position.y, player.size.x, player.size.y)
        batch.end()

        //println(Comun.battleMessages)
        actionLog.draw()
        /*if (!Comun.battleMessages.isEmpty()){
            //println(Comun.battleMessages)
        }*/
        //println((1/delta).toString()) //this line prints out our fps
    }

    override fun pause() {
    }

    override fun resume() {
    }

    override fun resize(width: Int, height: Int) {
        gamePort.update(width, height)
        gamePort.camera.update()
    }

    override fun hide() {
    }

    override fun show() {
    }

    override fun dispose() {
    }
}