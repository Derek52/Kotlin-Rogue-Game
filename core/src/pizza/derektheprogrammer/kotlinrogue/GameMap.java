/*
package pizza.derektheprogrammer.kotlinrogue;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import java.util.ArrayList;
import java.util.Random;
import pizza.derektheprogrammer.kotlinrogue.entities.Player;

public class GameMap {

    private float worldWidth;
    private float worldHeight;

    public Tile[][] tiles;

    public GameMap(float width, float height) {
        worldWidth = width;
        worldHeight = height;

        tiles = new Tile[(int)width][(int)height];
    }

    public void initializeTiles() {
        tiles = new Tile[(int)worldWidth][(int)worldHeight];
        tiles = new Tile[50][50];
        //Random RNG = new Random();

        */
/*for (int x = 0; x < (int)worldWidth; x++) {
            for (int y = 0; y < (int)worldHeight; y++) {
                if (RNG.nextBoolean()) {
                    tiles[x][y] = new Tile((float)x, (float)y, true, true);
                } else {
                    tiles[x][y] = new Tile((float)x, (float)y, false, false);
                }
            }
        }*//*

        for (int i = 0; i <= 10; i++) {

        }
        for (int x = 0; x < (int)worldWidth; x++) {
            for (int y = 0; y < (int)worldHeight; y++) {
                tiles[x][y] = new Tile((float)x, (float)y, true, true);
            }
        }
    }

    public void toKotlin() {
        Tile[][] board = new Tile[50][50];
        for (int i = 0; i < 50; i++) {
            for (int j = 0; j < 50; j++) {
                board[i][j] = new Tile(i, j, false, false, false, false);
            }
        }
    }


    public void makeMap(int maxRooms, int roomMinSize, int roomMaxSize, int mapWidth, int mapHeight, Player player) {
        initializeTiles();
        ArrayList<Rectangle> rooms = new ArrayList<Rectangle>();
        Random RNG = new Random();
        boolean overlapped;

        for (int i = 0; i < maxRooms; i++) {
            overlapped = false;
	        int width = RNG.nextInt((roomMaxSize - roomMinSize) + 1) + roomMinSize;
	        int height = RNG.nextInt((roomMaxSize - roomMinSize) + 1) + roomMinSize;
            //int width = RNG.nextInt(roomMinSize) + (roomMaxSize - roomMinSize);
            //int height = RNG.nextInt(roomMinSize) + (roomMaxSize - roomMinSize);
            int xPos = RNG.nextInt(mapWidth - width);
            int yPos = RNG.nextInt(mapHeight - height);

            Rectangle newRoom = new Rectangle(xPos, yPos, width, height);
            System.out.println("Tried making room " + String.valueOf(i));
            for (Rectangle room : rooms) {
                if (Intersector.overlaps(newRoom, room)) {
                    System.out.println("Room " + i + "overlapped");
                    overlapped = true;
                    break;
                }
            }

            if (!overlapped) {
                createRoom(newRoom);

                Vector2 newCenter = getCenter(newRoom);

                if (rooms.size() == 0) {
                    player.getPosition().x = newCenter.x;
                    player.getPosition().y = newCenter.y;
                } else {
                    Vector2 prevCenter = getCenter(rooms.get(rooms.size()-1));

                    if (RNG.nextBoolean()) {
                        createHorizontalTunnel(prevCenter.x, newCenter.x, prevCenter.y);
                        createVerticalTunnel(prevCenter.y, newCenter.y, newCenter.x);
                    } else {
                        createVerticalTunnel(prevCenter.y, newCenter.y, prevCenter.x);
                        createHorizontalTunnel(prevCenter.x, newCenter.x, newCenter.y);
                    }
                }
                rooms.add(newRoom);
            }
        }
        System.out.println(rooms.size());
    }

    */
/*public void makeMap(int maxRooms, int roomMinSize, int roomMaxSize, int mapWidth, int mapHeight, Player player) {
        initializeTiles();
        ArrayList<Rectangle> rooms = new ArrayList<Rectangle>();
        Random RNG = new Random();

        for (int i = 0; i < maxRooms; i++) {
            boolean blocking = false;
            int width = RNG.nextInt(roomMinSize) + (roomMaxSize - roomMinSize);
            int height = RNG.nextInt(roomMinSize) + (roomMaxSize - roomMinSize);
            int xPos = RNG.nextInt(mapWidth - width - 1);
            int yPos = RNG.nextInt(mapHeight - height - 1);

            Rectangle newRoom = new Rectangle(xPos, yPos, width, height);

            if (rooms.size() == 0) {
                Vector2 newRoomCenter = getCenter(newRoom);
                player.getPosition().x = newRoomCenter.x;
                player.getPosition().y = newRoomCenter.y;
            }
            else {
                for (Rectangle room : rooms) {
                    if (Intersector.overlaps(room, newRoom)) {
                        //blocking = true;
                        System.out.println("Testing for intersections attempt " + String.valueOf(i));
                        break;
                    }
                }
                System.out.println("Creating room number" + String.valueOf(i));
                createRoom(newRoom);

                Vector2 previousRoomCenter = getCenter(rooms.get(rooms.size()-1));

                if (RNG.nextBoolean()) {
                    createHorizontalTunnel(previousRoomCenter.x, newRoom.x, previousRoomCenter.y);
                    createVerticalTunnel(previousRoomCenter.y, newRoom.y, newRoom.x);
                }
                else {
                    createVerticalTunnel(previousRoomCenter.y, newRoom.y, previousRoomCenter.x);
                    createHorizontalTunnel(previousRoomCenter.x, newRoom.x, newRoom.y);
                }

            }
            System.out.println("Adding room number " + String.valueOf(i));
            rooms.add(newRoom);
            *//*
*/
/*if (!blocking) {
                rooms.add(newRoom);
            }*//*
*/
/*
            //rooms.add(newRoom);
        }
        System.out.println(rooms.size());
    }*//*


    */
/*public void makeMap(int maxRooms, int roomMinSize, int roomMaxSize, int mapWidth, int mapHeight, Player player) {
        Rectangle[] rooms = new Rectangle[maxRooms];
        int roomCount = 0;
        rooms[0] = new Rectangle(0f, 0f, 1f, 1f);
        Random RNG = new Random();

        for (int i = 0; i < maxRooms; i++) {
            int width = RNG.nextInt(roomMinSize) + (roomMaxSize - roomMinSize);
            int height = RNG.nextInt(roomMinSize) + (roomMaxSize - roomMinSize);
            int xPos = RNG.nextInt(mapWidth - width - 1);
            int yPos = RNG.nextInt(mapHeight - height - 1);

            Rectangle newRoom = new Rectangle(xPos, yPos, width, height);

            if (roomCount == 0) {
                Vector2 newRoomCenter = getCenter(newRoom);
                player.getPosition().x = newRoomCenter.x;
                player.getPosition().y = newRoomCenter.y;
            }
            else {
                for (Rectangle room : rooms) {Vector2 newRoomCenter = getCenter(newRoom);
                    if (Intersector.overlaps(newRoom, room)) {
                        break;
                    }
                    else {
                        createRoom(newRoom);

                        Vector2 previousRoomCenter = getCenter(rooms[roomCount - 1]);

                        if (RNG.nextBoolean()) {
                            createHorizontalTunnel(previousRoomCenter.x, newRoom.x, previousRoomCenter.y);
                            createVerticalTunnel(previousRoomCenter.y, newRoom.y, newRoom.x);
                        }
                        else {
                            createVerticalTunnel(previousRoomCenter.y, newRoom.y, previousRoomCenter.x);
                            createHorizontalTunnel(previousRoomCenter.x, newRoom.x, newRoom.y);
                        }

                    }
                }
            }

            rooms[i] = newRoom;
            roomCount++;
        }

    }*//*


    public Vector2 getCenter(Rectangle room) {
        float centerX = room.x + (room.width/2);
        float centerY = room.y + (room.height/2);
        return new Vector2(centerX, centerY);
    }

    */
/*fun getCenter(rect : Rectangle) : Vector2 {
        val centerX = rect.x + (rect.width / 2)
        val centerY = rect.y + (rect.height / 2)
        return Vector2(centerX, centerY)
    }*//*


    public void createRoom(Rectangle room) {
        for (int x = (int)room.x+1; x < (int)room.x+room.width; x++) {
            for (int y = (int)room.y+1; y < (int)room.y+room.height; y++) {
                tiles[x][y].setBlocked(false);
                tiles[x][y].setBlockSight(false);
            }
        }
    }

    public void createHorizontalTunnel(float x1, float x2, float y) {
        int yI = (int)y; //yI is yInt
        if (x1 > x2) {
            for (int i = (int)x2; i < (int)x1 + 1; i++) {
                tiles[i][yI].setBlocked(false);
                tiles[i][yI].setBlockSight(false);
            }
        } else {
            for (int i = (int)x1; i < (int)x2 + 1; i++) {
                tiles[i][yI].setBlocked(false);
                tiles[i][yI].setBlockSight(false);
            }
        }
    }

    public void createVerticalTunnel(float y1, float y2, float x) {
        int xI = (int)x; //xI is yInt
        if (y1 > y2) {
            for (int i = (int)y2; i < (int)y1 + 1; i++) {
                tiles[xI][i].setBlocked(false);
                tiles[xI][i].setBlockSight(false);
            }
        } else {
            for (int i = (int)y1; i < (int)y2 + 1; i++) {
                tiles[xI][i].setBlocked(false);
                tiles[xI][i].setBlockSight(false);
            }
        }
    }
}
*/
