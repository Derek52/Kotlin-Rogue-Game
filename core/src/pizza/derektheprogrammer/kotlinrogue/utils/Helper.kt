package pizza.derektheprogrammer.kotlinrogue.utils

import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2

object Helper {

    //these methods were added in the rect.py class in the rogueliketutorial. But, I'm using LibGDX's rectangle class, so I added these methods in this static class

    fun getCenter(rect : Rectangle) : Vector2 {
        val centerX = rect.x + (rect.width / 2)
        val centerY = rect.y + (rect.height / 2)
        return Vector2(centerX, centerY)
    }

    //Libgdx has a built in method for this. But, I went ahead and copied this method here for the hell of it.
    /*fun intersect(rect1 : Rectangle, rect2 : Rectangle) : Boolean {
        return (rect1.x <= rect2.x + rect2.width && rect1.x + rect1.width >= rect2.x
                && rect1.y <= rect2.y + rect2.height && rect1.y + rect1.height >= rect2.y)
    }*/
}