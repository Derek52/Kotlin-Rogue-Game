package pizza.derektheprogrammer.kotlinrogue.utils
//this is an object that stores some of our gameplay variables.
//This lets you go to one file, to tweak the settings for multiple gameplay elements. E.g., PLAYER_SPEED, world size, enemy spawn rates, whatever.
//Other people would name this file something like Definitions. I name it Comun, which is spanish for common. Don't ask me why. I've forgotten.
object Comun {

    //I follow the naming convention of using all caps for constants.

    enum class GameState {
        PLAYERS_TURN,
        ENEMY_TURN
    }

    const val MAP_WIDTH = 80f
    const val MAP_HEIGHT = 50f
    //const val HUD_HEIGHT = 5f
    const val HUD_BAR_WIDTH = 30f

    const val PLAYER_SPEED = 10f

    const val ROOM_MAX_SIZE = 10
    const val ROOM_MIN_SIZE = 6
    const val MAX_ROOM_NUMBER = 30

    const val MAX_MONSTERS_PER_ROOM = 3
    const val MAX_ITEMS_PER_ROOM = 2

    const val FOV_RADIUS = 4.5f

    const val ENEMY_FOV_RADIUS = 4f
    const val ENEMY_SPEED = 2f
    const val ENEMY_SIZE = .7f

    val battleMessages = ArrayList<String>()
    //var battleMessages = ""

}