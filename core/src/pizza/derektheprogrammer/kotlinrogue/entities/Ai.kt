package pizza.derektheprogrammer.kotlinrogue.entities

import com.badlogic.gdx.math.Circle
import com.badlogic.gdx.math.Intersector
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import pizza.derektheprogrammer.kotlinrogue.GameMap
import pizza.derektheprogrammer.kotlinrogue.utils.Comun

class Ai {

    lateinit var owner : Entity

    val fovCircle = Circle(0f, 0f, Comun.ENEMY_FOV_RADIUS)

    fun takeTurn(player : Player) {
        owner.velocity.set(0f, 0f)
        fovCircle.setPosition(owner.position.x + owner.hitBox.width/2, owner.position.y + owner.hitBox.height/2)

        if (Intersector.overlaps(fovCircle, player.hitBox)) {
            moveTowards(player.position)
        }
        //println(owner.name)
    }

    private fun moveTowards(targetPosition : Vector2) {
        if (owner.position.x > targetPosition.x) {
            owner.velocity.x = -Comun.ENEMY_SPEED
        } else if (owner.position.x < targetPosition.x) {
            owner.velocity.x = Comun.ENEMY_SPEED
        } else {
            owner.velocity.x = 0f
        }

        /*if ((owner.position.y > targetPosition.y && owner.position.y < targetPosition.y + .05f) ||
                (owner.position.y < targetPosition.y && owner.position.y > targetPosition.y - .05f)) {
            owner.velocity.y = 0f
        } */if (owner.position.y > targetPosition.y) {
            owner.velocity.y = -Comun.PLAYER_SPEED
        } else if (owner.position.y < targetPosition.y) {
            owner.velocity.y = Comun.PLAYER_SPEED
        } else {
            owner.velocity.y = 0f
        }
    }
}