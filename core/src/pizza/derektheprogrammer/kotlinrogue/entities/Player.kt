package pizza.derektheprogrammer.kotlinrogue.entities

import com.badlogic.gdx.math.Circle
import com.badlogic.gdx.math.Intersector
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import org.jetbrains.kotlin.utils.identity
import pizza.derektheprogrammer.kotlinrogue.GameMap
import pizza.derektheprogrammer.kotlinrogue.Tile
import pizza.derektheprogrammer.kotlinrogue.utils.Comun

class Player(val position : Vector2, val size : Vector2 = Vector2(), val velocity : Vector2 = Vector2(),
             val hitBox : Rectangle = Rectangle(position.x, position.y, size.x, size.y),
             val fighter : Fighter) {

    var hasMoved = true
    var fovCircle = Circle(position.x, position.y, Comun.FOV_RADIUS)

    fun update(dt : Float, gameMap: GameMap) {
        hasMoved = false
        velocity.scl(dt)

        hitBox.x += velocity.x

        var collidedWithBlockedTile = false

        for (x in 0 until Comun.MAP_WIDTH.toInt()) {
            for (y in 0 until Comun.MAP_HEIGHT.toInt()) {
                if (gameMap.tiles[x][y].blocked) {
                    if (Intersector.overlaps(gameMap.tiles[x][y].rect, hitBox)) {
                        collidedWithBlockedTile = true
                    }
                }
            }
        }

        for (i in gameMap.entities.size - 1 downTo 0) {
            if (Intersector.overlaps(gameMap.entities[i].hitBox, hitBox)) {
                collidedWithBlockedTile = true
                println("Collided with enemy #$i")
                gameMap.entities[i].fighter?.let {
                    gameMap.entities[i].fighter!!.takeDamage(10, gameMap.entities[i].name)
                    Comun.battleMessages.add("\nYou damage ${gameMap.entities[i].name} for 10 damage")
                }

                if (gameMap.entities[i].isItem) {
                    Comun.battleMessages.add("\nPotion heals 100hp")
                    fighter.hp += 100
                    gameMap.entities.removeAt(i)
                }
            }
        }

        if (collidedWithBlockedTile) {
            hitBox.x = position.x
        } else {
            position.x = hitBox.x
            hasMoved = true
        }
        collidedWithBlockedTile = false

        hitBox.y += velocity.y
        for (x in 0 until Comun.MAP_WIDTH.toInt()) {
            for (y in 0 until Comun.MAP_HEIGHT.toInt()) {
                if (gameMap.tiles[x][y].blocked) {
                    if (Intersector.overlaps(gameMap.tiles[x][y].rect, hitBox)) {
                        collidedWithBlockedTile = true
                    }
                }
            }
        }

        for (i in gameMap.entities.size - 1 downTo 0) {
            if (Intersector.overlaps(gameMap.entities[i].hitBox, hitBox)) {
                collidedWithBlockedTile = true
                println("Collided with enemy #$i")
                gameMap.entities[i].fighter?.let {
                    gameMap.entities[i].fighter!!.takeDamage(10, gameMap.entities[i].name)
                    Comun.battleMessages.add("\nYou damage ${gameMap.entities[i].name} for 10 damage")
                }

                if (gameMap.entities[i].isItem) {
                    Comun.battleMessages.add("\nPotion heals 100hp")
                    fighter.hp += 100
                    gameMap.entities.removeAt(i)
                }
            }
        }

        if (collidedWithBlockedTile) {
            hitBox.y = position.y
        } else {
            position.y = hitBox.y
            hasMoved = true
        }
        velocity.scl(1/dt)

        if (hasMoved) {
            computeFOV(gameMap.tiles)
        }
    }

    fun computeFOV(tiles : Array<Array<Tile>>) {
        fovCircle.x = position.x
        fovCircle.y = position.y
        for (x in 0 until Comun.MAP_WIDTH.toInt()) {
            for (y in 0 until Comun.MAP_HEIGHT.toInt()) {
                if (Intersector.overlaps(fovCircle, tiles[x][y].rect)) {
                    tiles[x][y].visible = true
                    tiles[x][y].illuminated = true
                }
                if (tiles[x][y].illuminated && (!Intersector.overlaps(fovCircle, tiles[x][y].rect))) {
                    tiles[x][y].illuminated = false
                }
            }
        }
    }
}