package pizza.derektheprogrammer.kotlinrogue.entities

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Intersector
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import pizza.derektheprogrammer.kotlinrogue.GameMap
import pizza.derektheprogrammer.kotlinrogue.utils.Comun

class Entity(val texture : Texture, val name : String, var position : Vector2 = Vector2(0f, 0f), val size : Vector2, val velocity : Vector2 = Vector2(0f, 0f),
             var blocks : Boolean = false, val fighter : Fighter? = null, val ai : Ai? = null, val isItem : Boolean = false) {

    var hitBox : Rectangle

    init {
        hitBox = Rectangle(position.x, position.y, size.x, size.y)
    }

    fun update(dt : Float, gameMap : GameMap, player : Player) {
        var collidedWithSomething = false

        hitBox.x += velocity.x * dt

        for (entity in gameMap.entities) {
            if (entity == this){
                continue
            }
            if (Intersector.overlaps(entity.hitBox, hitBox)) {
                collidedWithSomething = true
            }
        }

        for (x in 0 until Comun.MAP_WIDTH.toInt()) {
            for (y in 0 until Comun.MAP_HEIGHT.toInt()) {
                if (gameMap.tiles[x][y].blocked) {
                    if (Intersector.overlaps(gameMap.tiles[x][y].rect, hitBox)) {
                        collidedWithSomething = true
                    }
                }
            }
        }

        if (Intersector.overlaps(player.hitBox, hitBox)) {
            collidedWithSomething = true
            player.fighter.takeDamage(5)
            Comun.battleMessages.add("\n$name hits you for 5 damage, your hp is ${player.fighter.hp}")
            //println("$name hits you for 5 damage, your hp is ${player.fighter.hp}")
        }

        if (collidedWithSomething) {
            hitBox.x = position.x
            collidedWithSomething = false
        } else {
            position.x = hitBox.x
        }


        hitBox.y += velocity.y * dt

        for (entity in gameMap.entities) {
            if (entity == this){
                continue
            }
            if (Intersector.overlaps(entity.hitBox, hitBox)) {
                collidedWithSomething = true
            }
        }

        for (x in 0 until Comun.MAP_WIDTH.toInt()) {
            for (y in 0 until Comun.MAP_HEIGHT.toInt()) {
                if (gameMap.tiles[x][y].blocked) {
                    if (Intersector.overlaps(gameMap.tiles[x][y].rect, hitBox)) {
                        collidedWithSomething = true
                    }
                }
            }
        }

        if (Intersector.overlaps(player.hitBox, hitBox)) {
            collidedWithSomething = true
            player.fighter.takeDamage(5)
            Comun.battleMessages.add("\n$name hits you for 5 damage, your hp is ${player.fighter.hp}")
            //println("$name hits you for 5 damage, your hp is ${player.fighter.hp}")
        }

        if (collidedWithSomething) {
            hitBox.y = position.y
        } else {
            position.y = hitBox.y
        }
    }

    fun draw(batch : SpriteBatch) {
	    batch.draw(texture, position.x, position.y, size.x, size.y)
    }

}
