package pizza.derektheprogrammer.kotlinrogue.entities

class Fighter(val maxHp : Int, var hp : Int, val defense : Int, val power : Int, var dead : Boolean = false) {

    fun takeDamage(amountOfDamage : Int) {
        hp -= amountOfDamage
        if (hp <= 0) {
            dead = true
        }
    }

    fun takeDamage(amountOfDamage: Int, entityNumber : String) {
        hp -= amountOfDamage
        println("$entityNumber has taken $amountOfDamage damage")
        if (hp <= 0) {
            dead = true
        }
    }

    //This is the start of a more complicated battle system. It's only more complicated because the above method has 0 complexity
    /*fun takeDamage(enemyPower : Int) {
        hp -= defense - enemyPower
    }*/
}