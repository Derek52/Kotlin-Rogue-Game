package pizza.derektheprogrammer.kotlinrogue

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.math.Intersector
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import pizza.derektheprogrammer.kotlinrogue.entities.Ai
import pizza.derektheprogrammer.kotlinrogue.entities.Entity
import pizza.derektheprogrammer.kotlinrogue.entities.Fighter
import pizza.derektheprogrammer.kotlinrogue.entities.Player
import pizza.derektheprogrammer.kotlinrogue.utils.Comun
import java.util.*

class GameMap(private val width : Float, private val height : Float) {

    var tiles : Array<Array<Tile>>
    var entities = ArrayList<Entity>()
    private val RNG = Random()
    var j = 0

    init {
        //this line of code took some experimenting to finally get right. This is the one thing that looked way nicer in Java
        tiles = Array(width.toInt()) { Array(height.toInt()) { Tile(0f, 0f, false) } }
    }

    private fun initializeTiles() {
        tiles = Array(width.toInt()) { Array(height.toInt()) { Tile(0f, 0f, false) } }

        for (x in 0 until width.toInt()) {
            for (y in 0 until height.toInt()) {
                tiles[x][y] = Tile(x.toFloat(), y.toFloat(), true)
            }
        }

    }

    fun update() {
        for (i in entities.size - 1 downTo 0) {
            entities[i].fighter?.let {
                if (entities[i].fighter!!.dead) {
                    println("Entity #$i is dead")
                }
                //println("Entity $i hp=${entities[i].fighter!!.hp} dead=${entities[i].fighter!!.dead}")
            }
        }
        for (i in entities.size - 1 downTo 0) {
            entities[i].fighter?.let {
                if (entities[i].fighter!!.dead) {
                    Comun.battleMessages += "\n${entities[i].name} has died"
                    entities.removeAt(i)
                }
            }
        }
    }

    fun makeMap(maxRooms : Int, roomMinSize : Int, roomMaxSize : Int, mapWidth : Int, mapHeight : Int, player : Player) {
        initializeTiles()
        entities.clear()
        val rooms = ArrayList<Rectangle>()
        var overlapped : Boolean

        for (i in 0 until maxRooms) {
            overlapped = false
            val width = RNG.nextInt((roomMaxSize - roomMinSize) + 1) + roomMinSize
            val height = RNG.nextInt((roomMaxSize - roomMinSize) + 1) + roomMinSize
            val xPos = RNG.nextInt(mapWidth - width)
            val yPos = RNG.nextInt(mapHeight - height)

            val newRoom = Rectangle(xPos.toFloat(), yPos.toFloat(), width.toFloat(), height.toFloat())

            for (room in rooms) {
                if (Intersector.overlaps(newRoom, room)) {
                    overlapped = true
                    break
                }
            }

            if (!overlapped) {
                createRoom(newRoom)

                val newCenter = getCenter(newRoom)

                if (rooms.size == 0) {
                    player.position.x = newCenter.x
                    player.position.y = newCenter.y
                } else {
                    val prevCenter = getCenter(rooms.last())
                    //val prevCenter = getCenter(rooms[rooms.size - 1])

                    if (RNG.nextBoolean()) {
                        createHorizontalTunnel(prevCenter.x, newCenter.x, prevCenter.y)
                        createVerticalTunnel(prevCenter.y, newCenter.y, newCenter.x)
                    } else {
                        createVerticalTunnel(prevCenter.y, newCenter.y, prevCenter.x)
                        createHorizontalTunnel(prevCenter.x, newCenter.x, newCenter.y)
                    }
                }
                spawnMonsters(newRoom, Comun.MAX_MONSTERS_PER_ROOM)
                spawnItems(newRoom, Comun.MAX_ITEMS_PER_ROOM)
                rooms.add(newRoom)
            }
        }
    }

    private fun spawnMonsters(room : Rectangle, maxMonstersPerRoom : Int) {
        val numberOfMonsters = RNG.nextInt(maxMonstersPerRoom + 1)

        //val fighterComponent = Fighter(20, 20, 5, 10)

        for (i in 0 until numberOfMonsters) {
            var entityOverlapped = false

            //python's randint() function is way neater for this, but I'm still happy with this.
            val x : Float = RNG.nextInt(((room.x + room.width - 1f).toInt() - (room.x + 1f).toInt()) + 1) + room.x + 1
            val y : Float = RNG.nextInt(((room.y + room.height - 1f).toInt() - (room.y + 1f).toInt()) + 1) + room.y + 1

            val newEntityHitbox = Rectangle(x, y, 1f, 1f)
            for (entity in entities) {
                if (Intersector.overlaps(entity.hitBox, newEntityHitbox)) {
                    entityOverlapped = true
                }
            }

            if (!entityOverlapped) {
                val fighterComponent = Fighter(20, 20, 5, 10)
                val entity = Entity(Texture("orc.png"), "Orc$j", Vector2(x, y), Vector2(Comun.ENEMY_SIZE, Comun.ENEMY_SIZE), fighter = fighterComponent, ai = Ai())
                j++
                println(j)
                entity.ai!!.owner = entity
                entities.add(entity)
            }
        }
    }

    private fun spawnItems(room : Rectangle, maxItemsPerRoom : Int) {
        val numberOfItems = RNG.nextInt(maxItemsPerRoom + 1)

        for (i in 0 until numberOfItems) {
            var entityOverlapped = false

            val x : Float = RNG.nextInt(((room.x + room.width - 1f).toInt() - (room.x + 1f).toInt()) + 1) + room.x + 1
            val y : Float = RNG.nextInt(((room.y + room.height - 1f).toInt() - (room.y + 1f).toInt()) + 1) + room.y + 1

            val newEntityHitbox = Rectangle(x, y, 1f, 1f)
            for (entity in entities) {
                if (Intersector.overlaps(entity.hitBox, newEntityHitbox)) {
                    entityOverlapped = true
                }
            }

            if (!entityOverlapped) {
                val entity = Entity(Texture("pink_potion.png"), "potion", Vector2(x, y), Vector2(.8f, 1f), isItem = true)
                entities.add(entity)
            }
        }
    }

    private fun createRoom(room : Rectangle) {
        for (x in (room.x.toInt()+1) until (room.x+room.width).toInt()) {
            for (y in (room.y.toInt()+1) until (room.y+room.height).toInt()) {
                tiles[x][y].blocked = false
                tiles[x][y].blockSight = false
            }
        }
    }

    private fun getCenter(room : Rectangle) : Vector2 {
        val centerX = room.x + (room.width/2)
        val centerY = room.y + (room.height/2)
        return Vector2(centerX, centerY)
    }

    private fun createHorizontalTunnel(x1 : Float, x2 : Float, y : Float) {
        val yI = y.toInt()//store the int value of Y, so I can use it in the for loop. I'm not sure if this is even a necessary optimization.
        for (i in Math.min(x1, x2).toInt()..Math.max(x1, x2).toInt()) {
            tiles[i][yI].blocked = false
            tiles[i][yI].blockSight = false
        }
    }

    private fun createVerticalTunnel(y1 : Float, y2 : Float, x : Float) {
        val xI = x.toInt()
        for (i in Math.min(y1, y2).toInt()..Math.max(y1, y2).toInt()) {
            tiles[xI][i].blocked = false
            tiles[xI][i].blockSight = false
        }
    }
}


//random generation V.01, didn't feel like deleting it.
/*for (x in 0..width.toInt()) {
            for (y in 0..width.toInt()) {
                if (RNG.nextBoolean()) {
                    tiles[x][y] = Tile(x.toFloat(), y.toFloat(), true)
                } else {
                    tiles[x][y] = Tile(x.toFloat(), y.toFloat(), false)
                }
            }
        }*/