package pizza.derektheprogrammer.kotlinrogue.scenes

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.utils.Disposable
import com.badlogic.gdx.utils.viewport.FitViewport
import pizza.derektheprogrammer.kotlinrogue.utils.Comun

class ActionLog(private val batch : SpriteBatch) : Disposable{

    val stage : Stage
    val viewPort : FitViewport

    val messagesLabel : Label
    val hpLabel : Label

    init {
        viewPort = FitViewport(800f, 600f, OrthographicCamera())
        stage = Stage(viewPort, batch)

        messagesLabel = Label("", Label.LabelStyle(BitmapFont(), Color.WHITE))
        hpLabel = Label("", Label.LabelStyle(BitmapFont(), Color.WHITE))

        val logTable = Table()
        //logTable.left().top()
        logTable.setFillParent(true)
        logTable.right().bottom()
        //logTable.add(hpLabel).row()
        logTable.add(messagesLabel).row()


        val hudTable = Table()
        hudTable.left().top()
        hudTable.setFillParent(true)

        hudTable.add(hpLabel)

        stage.addActor(logTable)
        stage.addActor(hudTable)
    }

    fun update(hp : Int) {
        hpLabel.setText("HP: $hp")
    }

    fun draw() {
        batch.projectionMatrix = viewPort.camera.combined
        while (Comun.battleMessages.size > 5) {
            Comun.battleMessages.removeAt(0)
        }
        var messages = ""
        for (message in Comun.battleMessages) {
            messages += message
        }
        messagesLabel.setText(messages)
        stage.draw()
    }

    override fun dispose() {
        stage.dispose()
    }
}