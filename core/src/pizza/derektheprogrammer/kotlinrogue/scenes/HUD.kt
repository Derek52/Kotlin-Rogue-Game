/*
package pizza.derektheprogrammer.kotlinrogue.scenes

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.utils.viewport.FitViewport
import pizza.derektheprogrammer.kotlinrogue.utils.Comun

//I was going to use this class to draw a health bar, but got lazy, and am just going to use ActionLog to display the player's hp as a number.
//For now at least

class HUD(val position : Float, val maximumWidth : Int) {

    val viewPort : FitViewport

    val barWidth = Comun.HUD_BAR_WIDTH

    init {
        viewPort = FitViewport(800f, 600f, OrthographicCamera())

    }

    fun draw(shapeRenderer : ShapeRenderer) {
        shapeRenderer.color = Color.ROYAL
        shapeRenderer.rect(1f, position, barWidth, 1f)
    }
}*/
