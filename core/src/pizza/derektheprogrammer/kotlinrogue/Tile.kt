package pizza.derektheprogrammer.kotlinrogue

import com.badlogic.gdx.math.Rectangle

class Tile(val posX : Float, val posY : Float, var blocked : Boolean, var blockSight : Boolean = blocked,
           var visible : Boolean = false, var illuminated : Boolean = false) {

    val rect : Rectangle

    init {
        rect = Rectangle(posX, posY, 1f, 1f)
    }
}